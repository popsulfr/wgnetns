# Simple Wireguard Network Namespace manager

Sets up a separate network namespace according to a wg-quick configuration file (`man 8 wg-quick`)

## Requirements

* bash
* wg
* wg-quick
* iproute2
* sudo

## Usage

Setup a network namespace

```
./wgnetns.sh start <wg-quick configuration file>
```
e.g.:

```
./wgnetns.sh start /etc/wireguard/myvpn.conf
```

Remove a network namespace

```
./wgnetns.sh stop <wg-quick configuration file>
```

e.g.:

```
./wgnetns.sh stop /etc/wireguard/myvpn.conf
```

Execute a command in the network namespace as root

```
./wgnetns.sh exec <wg-quick configuration file> cmds...
```
e.g.:

```
./wgnetns.sh exec /etc/wireguard/myvpn.conf wg
```

Execute a command in the network namespace as the current user

```
./wgnetns.sh exec_user <wg-quick configuration file> cmds...
```
e.g.:

```
./wgnetns.sh exec_user /etc/wireguard/myvpn.conf firefox
```

## firejail

Start the network namespace and use the name (configuration file name without `.conf`) as target namespace with firejail :

```
firejail --netns=myvpn firefox
```
