#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

my_def_dns=(1.1.1.1 8.8.8.8)

my_cmd() {
	echo '[#]' "${@}"
	"${@}"
}

# my_name_from_conf conf
my_name_from_conf() {
	basename "${1:?Missing configuration file}" .conf
}

my_addresses_from_conf() {
	sed -n 's/^Address\s*=\s*\(.\+\)/\1/p' < "${1:?Missing config file}" | sed 's/,/\n/g'
}

my_dns_from_conf() {
	sed -n 's/^DNS\s*=\s*\(.\+\)/\1/p' < "${1:?Missing conf file}" | sed 's/,/\n/g'
}

# my_start conf
my_start() {
	local conf name
	conf="${1:?Missing configuration file}"
	name="$(my_name_from_conf "${conf}")"

	my_cmd sudo ip netns del "${name}" || true
	my_cmd sudo ip netns add "${name}"
	my_cmd sudo ip link add dev "${name}" type wireguard
	my_cmd sudo ip link set "${name}" netns "${name}"
	for addr in $(my_addresses_from_conf "${conf}")
	do
		my_cmd sudo ip -n "${name}" addr add "${addr}" dev "${name}"
	done
	my_cmd sudo mkdir -p "/etc/netns/${name}"
	local dns
	mapfile -t dns < <(my_dns_from_conf "${conf}")
	[[ "${#dns[@]}" -eq 0 ]] && dns=("${my_def_dns[@]}")
	for d in "${dns[@]}"
	do
		echo "nameserver ${d}" | my_cmd sudo tee -a "/etc/netns/${name}/resolv.conf"
	done
	# shellcheck disable=SC2016
	my_cmd sudo ip netns exec "${name}" bash -c 'wg setconf "${1}" <(wg-quick strip "${2}")' _ "${name}" "${conf}"
	my_cmd sudo ip -n "${name}" link set "${name}" up
	my_cmd sudo ip -n "${name}" route add default dev "${name}"
}

# my_stop conf
my_stop() {
	local conf name
	conf="${1:?Missing configuration file}"
	name="$(my_name_from_conf "${conf}")"

	my_cmd sudo ip netns del "${name}" || true
	my_cmd sudo rm -f "/etc/netns/${name}/resolv.conf" || true
	my_cmd sudo rmdir "/etc/netns/${name}" || true
}

# my_exec conf command...
my_exec() {
	local conf name
	conf="${1:?Missing configuration file}"
	name="$(my_name_from_conf "${conf}")"
	shift

	my_cmd exec sudo ip netns exec "${name}" "${@}"
}

my_exec_user() {
	local conf name
	conf="${1:?Missing configuration file}"
	name="$(my_name_from_conf "${conf}")"
	shift

	my_cmd exec sudo -E ip netns exec "${name}" sudo -E -u "$(id -u -n)" -- "${@}"
}

# my_main cmd conf
my_main() {
	local cmd="${1:?Missing command}"
	local conf="${2:?Missing configuration file}"
	shift
	shift

	case "${cmd}" in
		start)
			my_start "${conf}"
			;;
		stop)
			my_stop "${conf}"
			;;
		exec)
			my_exec "${conf}" "${@}"
			;;
		exec_user)
			my_exec_user "${conf}" "${@}"
			;;
		*)
			echo "Unknown command '${cmd}'" 1>&2
			return 1
			;;
	esac
}

my_main "${@}"
